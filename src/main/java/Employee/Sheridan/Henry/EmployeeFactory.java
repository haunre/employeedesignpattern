/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Employee.Sheridan.Henry;

/**
 *
 * @author 123
 */
public class EmployeeFactory {
    
    
    private static EmployeeFactory factory;

    private EmployeeFactory() { 
    }
    
    public static EmployeeFactory getInstance(){ 
    {
        if (factory==null)
            factory = new EmployeeFactory();
    }
        return factory;
    }
    public Employee getEmployee(String id, double salary, int numberOfHoursWorked){
    if(salary<=1500){
        return new Employee(id, salary, numberOfHoursWorked);
    }
    if(salary==2500){
        return new Employee(id, salary, numberOfHoursWorked);
    }
    return null;
    }


    }