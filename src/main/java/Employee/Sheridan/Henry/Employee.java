/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Employee.Sheridan.Henry;

/**
 *
 * @author 123
 */
 public class Employee {
    
    private String id;
    private double salary;
    private  int numberOfHoursWorked;
    

    public Employee() {
    }

    public Employee(String id, double salary, int numberOfHoursWorked) {
        this.id = id;
        this.salary = salary;
        this.numberOfHoursWorked = numberOfHoursWorked;
       
    }

    public double getTotalWage() {
       
        return salary*numberOfHoursWorked;
    }
    
    

    

    public String getId() {
        return id;
    }

    public double getSalary() {
        return salary;
    }

    public int getNumberOfHoursWorked() {
        return numberOfHoursWorked;
    }
    
    public boolean isManager(){
        if(numberOfHoursWorked==40 && salary==2500){
           }
         return true;
    }
   
 }
          
    

