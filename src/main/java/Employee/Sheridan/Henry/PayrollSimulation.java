/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Employee.Sheridan.Henry;

/**
 *
 * @author 123
 */
public class PayrollSimulation {
     public static void main(String[] args) {
     
        EmployeeFactory factory = EmployeeFactory.getInstance();
        
        Employee employee1 = new Employee("992DE", 1250, 37);
        Employee employee2 = new Employee("991EF", 1700, 40);
     
        
        System.out.println("Employee ID" + employee1.getId() + " and total wage is " + employee1.getTotalWage());
        System.out.println("it is " + employee1.isManager() + " this is the manager");
        
        System.out.println("Employee ID" + employee2.getId() + " and total wage is " + employee2.getTotalWage());
        System.out.println("it is " + employee2.isManager() + " this is the manager");
     }
    
}
